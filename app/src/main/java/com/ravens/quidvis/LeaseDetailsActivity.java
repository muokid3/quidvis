package com.ravens.quidvis;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.LeaseAdapter;
import com.ravens.quidvis.adapters.PaymentsAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.Lease;
import com.ravens.quidvis.models.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LeaseDetailsActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;
    public static String LEASE_ENDPOINT = "http://demo.ravens.co.ke/api/lease/load/";
    SweetAlertDialog pDialog, eDialog;
    List<Lease> leaseList;
    ListView listView;
    //TextView countText;
    StringBuilder stringBuilder;
    String leaseUrl;
    Dialog dialog;
    Button payNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lease_details);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userLocalStore = new UserLocalStore(this);

        leaseList = new ArrayList<>();
        payNow = (Button)findViewById(R.id.pay);

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LeaseDetailsActivity.this, PayActivity.class));
                finish();
            }
        });

        listView = (ListView) findViewById(R.id.landlord_list);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(LeaseDetailsActivity.this, Welcome.class));
                finish();
            }
        });

        pDialog.show();

        fetchLeaseDetails();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }



    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(LeaseDetailsActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private void fetchLeaseDetails() {

        stringBuilder = new StringBuilder(LEASE_ENDPOINT);

        stringBuilder.append(userLocalStore.getLoggedInUser().getUniqTenantId());

        leaseUrl = stringBuilder.toString();

        StringRequest req = new StringRequest(Request.Method.GET, leaseUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONObject responseObject = new JSONObject(response);


                            try
                            {
                                String surname = responseObject.getString("surname");
                                Lease surnameItem = new Lease("Surname: ", surname);
                                leaseList.add(surnameItem);

                            }
                            catch (JSONException je)
                            {
                                String surname = "N/A";
                                Lease surnameItem = new Lease("Surname: ", surname);
                                leaseList.add(surnameItem);

                            }

                            try
                            {
                                String town = responseObject.getString("town");
                                Lease townItem = new Lease("Town: ", town);
                                leaseList.add(townItem);

                            }
                            catch (JSONException je)
                            {
                                String town = "N/A";
                                Lease townItem = new Lease("Town: ", town);
                                leaseList.add(townItem);

                            }

                            try
                            {
                                String country = responseObject.getString("country");
                                Lease countryItem = new Lease("Country: ", country);
                                leaseList.add(countryItem);

                            }
                            catch (JSONException je)
                            {
                                String country = "N/A";
                                Lease countryItem = new Lease("Country: ", country);
                                leaseList.add(countryItem);

                            }

                            try
                            {
                                String accountNo = responseObject.getString("accountNo");
                                Lease accountNoItem = new Lease("Account Number: ", accountNo);
                                leaseList.add(accountNoItem);

                            }
                            catch (JSONException je)
                            {
                                String accountNo = "N/A";
                                Lease accountNoItem = new Lease("Account Number: ", accountNo);
                                leaseList.add(accountNoItem);

                            }

                            try
                            {
                                String pinNo = responseObject.getString("pinNo");
                                Lease pinNoItem = new Lease("PIN Number: ", pinNo);
                                leaseList.add(pinNoItem);

                            }
                            catch (JSONException je)
                            {
                                String pinNo = "N/A";
                                Lease pinNoItem = new Lease("PIN Number: ", pinNo);
                                leaseList.add(pinNoItem);

                            }

                            try
                            {
                                String idNo = responseObject.getString("idNo");
                                Lease idNoItem = new Lease("ID Number:", idNo);
                                leaseList.add(idNoItem);

                            }
                            catch (JSONException je)
                            {
                                String idNo = "N/A";
                                Lease idNoItem = new Lease("ID Number:", idNo);
                                leaseList.add(idNoItem);

                            }

                            try
                            {
                                int rentAmount = responseObject.getInt("rentAmount");
                                Lease rentAmountItem = new Lease("Rent Amount: ", rentAmount+"");
                                leaseList.add(rentAmountItem);

                            }
                            catch (JSONException je)
                            {
                                int rentAmount = 0;
                                Lease rentAmountItem = new Lease("Rent Amount: ", rentAmount+"");
                                leaseList.add(rentAmountItem);

                            }

                            try
                            {
                                String leaseType = responseObject.getString("leaseType");
                                Lease leaseTypeItem = new Lease("Lease Type: ", leaseType);
                                leaseList.add(leaseTypeItem);

                            }
                            catch (JSONException je)
                            {
                                String leaseType = "N/A";
                                Lease leaseTypeItem = new Lease("Lease Type: ", leaseType);
                                leaseList.add(leaseTypeItem);

                            }

                            try
                            {
                                String paymentFreq = responseObject.getString("paymentFreq");
                                Lease paymentFreqItem = new Lease("Payment Frequency: ", paymentFreq);
                                leaseList.add(paymentFreqItem);

                            }
                            catch (JSONException je)
                            {
                                String paymentFreq = "N/A";
                                Lease paymentFreqItem = new Lease("Payment Frequency: ", paymentFreq);
                                leaseList.add(paymentFreqItem);

                            }

                            try
                            {
                                String startDate = responseObject.getString("startDate");
                                Lease startDateItem = new Lease("Start Date: ", startDate);
                                leaseList.add(startDateItem);

                            }
                            catch (JSONException je)
                            {
                                String startDate = "N/A";
                                Lease startDateItem = new Lease("Start Date: ", startDate);
                                leaseList.add(startDateItem);

                            }

                            try
                            {
                                String endDate = responseObject.getString("endDate");
                                Lease endDateItem = new Lease("End Date:", endDate);
                                leaseList.add(endDateItem);

                            }
                            catch (JSONException je)
                            {
                                String endDate = "N/A";
                                Lease endDateItem = new Lease("End Date:", endDate);
                                leaseList.add(endDateItem);

                            }

                            try
                            {
                                String unitNo = responseObject.getString("unitNo");
                                Lease unitNoItem = new Lease("Unit Number: ", unitNo);
                                leaseList.add(unitNoItem);

                            }
                            catch (JSONException je)
                            {
                                String unitNo = "N/A";
                                Lease unitNoItem = new Lease("Unit Number: ", unitNo);
                                leaseList.add(unitNoItem);

                            }

                            try
                            {
                                String propName = responseObject.getString("propName");
                                Lease propNameItem = new Lease("Property Name: ", propName);
                                leaseList.add(propNameItem);

                            }
                            catch (JSONException je)
                            {
                                String propName = "N/A";
                                Lease propNameItem = new Lease("Property Name: ", propName);
                                leaseList.add(propNameItem);

                            }

                            try
                            {
                                String occupationStatus = responseObject.getString("occupationStatus");
                                Lease occupationStatusItem = new Lease("Occupation Status: ", occupationStatus);
                                leaseList.add(occupationStatusItem);

                            }
                            catch (JSONException je)
                            {
                                String occupationStatus = "N/A";
                                Lease occupationStatusItem = new Lease("Occupation Status: ", occupationStatus);
                                leaseList.add(occupationStatusItem);

                            }

                            try
                            {
                                String lrNo = responseObject.getString("lrNo");
                                Lease lrNoItem = new Lease("LR Number: ", lrNo);
                                leaseList.add(lrNoItem);

                            }
                            catch (JSONException je)
                            {
                                String lrNo = "N/A";
                                Lease lrNoItem = new Lease("LR Number: ", lrNo);
                                leaseList.add(lrNoItem);

                            }

                            try
                            {
                                int tenantId = responseObject.getInt("tenantId");
                                Lease tenantIdItem = new Lease("Tenant ID: ", tenantId+"");
                                leaseList.add(tenantIdItem);

                            }
                            catch (JSONException je)
                            {
                                int tenantId = 0;
                                Lease tenantIdItem = new Lease("Tenant ID: ", tenantId+"");
                                leaseList.add(tenantIdItem);

                            }

                            try
                            {
                                int leaseId = responseObject.getInt("leaseId");
                                Lease leaseIdItem = new Lease("Lease ID: ", leaseId+"");
                                leaseList.add(leaseIdItem);

                            }
                            catch (JSONException je)
                            {
                                int leaseId = 0;
                                Lease leaseIdItem = new Lease("Lease ID: ", leaseId+"");
                                leaseList.add(leaseIdItem);

                            }

                            try
                            {
                                int ledgerId = responseObject.getInt("ledgerId");
                                Lease ledgerIdItem = new Lease("Ledger ID: ", ledgerId+"");
                                leaseList.add(ledgerIdItem);

                            }
                            catch (JSONException je)
                            {
                                int ledgerId = 0;
                                Lease ledgerIdItem = new Lease("Ledger ID: ", ledgerId+"");
                                leaseList.add(ledgerIdItem);

                            }
                            try
                            {
                                int propLedgerId = responseObject.getInt("propLedgerId");
                                Lease propLedgerIdItem = new Lease("Property Ledger ID: ", propLedgerId+"");
                                leaseList.add(propLedgerIdItem);

                            }
                            catch (JSONException je)
                            {
                                int propLedgerId = 0;
                                Lease propLedgerIdItem = new Lease("Property Ledger ID: ", propLedgerId+"");
                                leaseList.add(propLedgerIdItem);

                            }
                            try
                            {
                                String rentPeriodVariation = responseObject.getString("rentPeriodVariation");
                                Lease rentPeriodVariationItem = new Lease("Rent Period Variation: ", rentPeriodVariation);
                                leaseList.add(rentPeriodVariationItem);


                            }
                            catch (JSONException je)
                            {
                                String rentPeriodVariation = "N/A";
                                Lease rentPeriodVariationItem = new Lease("Rent Period Variation: ", rentPeriodVariation);
                                leaseList.add(rentPeriodVariationItem);


                            }
                            try
                            {
                                String period = responseObject.getString("period");
                                Lease periodItem = new Lease("Period: ", period);
                                leaseList.add(periodItem);

                            }
                            catch (JSONException je)
                            {
                                String period = "N/A";
                                Lease periodItem = new Lease("Period: ", period);
                                leaseList.add(periodItem);

                            }
                            try
                            {
                                int daysToExpire = responseObject.getInt("daysToExpire");
                                Lease daysToExpireItem = new Lease("Days to Expire: ", daysToExpire+"");
                                leaseList.add(daysToExpireItem);

                            }
                            catch (JSONException je)
                            {
                                int daysToExpire = 0;
                                Lease daysToExpireItem = new Lease("Days to Expire: ", daysToExpire+"");
                                leaseList.add(daysToExpireItem);

                            }


                            ArrayAdapter<Lease> adapter = new LeaseAdapter(LeaseDetailsActivity.this, leaseList);
                            listView.setAdapter(adapter);

                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(LeaseDetailsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(LeaseDetailsActivity.this, Welcome.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(LeaseDetailsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, LeaseDetailsActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(LeaseDetailsActivity.this, Welcome.class));
                                finish();
                            }

                        })
                        .show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(LeaseDetailsActivity.this);
        requestQueue.add(req);
    }
}

package com.ravens.quidvis;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.PropertyListAdapter;
import com.ravens.quidvis.adapters.StatementsAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.Charge;
import com.ravens.quidvis.models.Landlord;
import com.ravens.quidvis.models.RentStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class StatementsActivity extends AppCompatActivity {
    UserLocalStore userLocalStore;
    public static String STATEMENTS_BASE_URL = "http://demo.ravens.co.ke/api/lease/statement";
    SweetAlertDialog pDialog, eDialog;
    List<RentStatement> statementList;
    ListView listView;
    //TextView countText;
    StringBuilder stringBuilder;
    String statementsUrl;
    Dialog dialog;
    Button payNow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statements);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userLocalStore = new UserLocalStore(this);
        statementList = new ArrayList<>();

        payNow = (Button)findViewById(R.id.pay);

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(StatementsActivity.this, PayActivity.class));
            }
        });

        listView = (ListView) findViewById(R.id.landlord_list);
        //countText = (TextView)findViewById(R.id.count);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(StatementsActivity.this, Welcome.class));
                finish();
            }
        });

        pDialog.show();

        fetchStatements();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                RentStatement rentStatement = (RentStatement) listView.getItemAtPosition(position);

                dialog = new Dialog(StatementsActivity.this);
                dialog.setContentView(R.layout.statement_details);

                TextView txnDate = (TextView) dialog.findViewById(R.id.txnDate);
                txnDate.setText("Transaction date: "+rentStatement.getTxnDate());

                TextView descr = (TextView) dialog.findViewById(R.id.descr);
                descr.setText("Description: "+rentStatement.getDescr());

                TextView accountName = (TextView) dialog.findViewById(R.id.accountName);
                accountName.setText("Account name: "+rentStatement.getAccountName());

                TextView txnType = (TextView) dialog.findViewById(R.id.txnType);
                txnType.setText("Transaction type: "+rentStatement.getTxnType());

                TextView credit = (TextView) dialog.findViewById(R.id.debit);
                credit.setText("Debit: "+rentStatement.getDebit());

                TextView balance = (TextView) dialog.findViewById(R.id.credit);
                balance.setText("Credit: "+rentStatement.getCredit());

                Button closeButton;
                closeButton = (Button) dialog.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(StatementsActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private void fetchStatements()
    {
        stringBuilder = new StringBuilder(STATEMENTS_BASE_URL);

        stringBuilder.append("?companyId="+userLocalStore.getLoggedInUser().getCompanyId());
        stringBuilder.append("&accountId="+userLocalStore.getLoggedInUser().getLedgerId());

        statementsUrl = stringBuilder.toString();

        StringRequest req = new StringRequest(Request.Method.GET, statementsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONObject responseObject = new JSONObject(response);
                            //int count = responseObject.getInt("count");
                            //countText.setText("Total: "+count);


                            JSONArray itemDetailsArray = responseObject.getJSONArray("list");

                            for (int i = 0; i < itemDetailsArray.length(); i++) {


                                JSONObject statementItem = (JSONObject) itemDetailsArray.get(i);
                                //Toast.makeText(StatementsActivity.this,landlordItem.toString(), Toast.LENGTH_LONG ).show();

                                String accountName = statementItem.getString("accountName");
                                String descr = statementItem.getString("descr");
                                String txnType = statementItem.getString("txnType");
                                String txnDate = statementItem.getString("txnDate");
                                int credit = statementItem.getInt("credit");
                                int debit = statementItem.getInt("debit");


                                RentStatement rentStatement = new RentStatement(accountName, descr, txnType, txnDate,credit,debit);

                                statementList.add(rentStatement);

                            }

                            ArrayAdapter<RentStatement> adapter = new StatementsAdapter(StatementsActivity.this, statementList);
                            listView.setAdapter(adapter);

                            pDialog.dismiss();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(StatementsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(StatementsActivity.this, Welcome.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(StatementsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, StatementsActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(StatementsActivity.this, Welcome.class));
                                finish();
                            }

                        })
                        .show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(StatementsActivity.this);
        requestQueue.add(req);

    }
}

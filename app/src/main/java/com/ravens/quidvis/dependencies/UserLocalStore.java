package com.ravens.quidvis.dependencies;

import android.content.Context;
import android.content.SharedPreferences;

import com.ravens.quidvis.models.User;

/**
 * Created by muoki on 2/1/2016.
 */
public class UserLocalStore {
    public static String SP_NAME = "userDetails";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context)
    {
        userLocalDatabase = context.getSharedPreferences(SP_NAME,0);
    }

    public void storeUserData(User user)
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("token", user.token);
        spEditor.putString("username", user.username);
        spEditor.putString("password", user.password);
        spEditor.putString("userId", user.userId);
        spEditor.putString("landlordId", user.landlordId);
        spEditor.putString("tenantId", user.tenantId);
        spEditor.putString("leaseId", user.leaseId);
        spEditor.putString("ledgerId", user.ledgerId);
        spEditor.putString("companyId", user.companyId);
        spEditor.putString("name", user.name);
        spEditor.putString("accountNo", user.accountNo);
        spEditor.putString("uniqTenantId", user.uniqTenantId);
        spEditor.commit();
    }

    public User getLoggedInUser()
    {
        String username = userLocalDatabase.getString("username","");
        String token = userLocalDatabase.getString("token","");
        String password = userLocalDatabase.getString("password","");
        String userId = userLocalDatabase.getString("userId","");
        String landlordId = userLocalDatabase.getString("landlordId","");
        String tenantId = userLocalDatabase.getString("tenantId","");
        String leaseId = userLocalDatabase.getString("leaseId","");
        String ledgerId = userLocalDatabase.getString("ledgerId","");
        String companyId = userLocalDatabase.getString("companyId","");
        String name = userLocalDatabase.getString("name","");
        String accountNo = userLocalDatabase.getString("accountNo","");
        String uniqTenantId = userLocalDatabase.getString("uniqTenantId","");


        User newUser = new User(username,password,token,userId,landlordId,tenantId,leaseId,ledgerId,companyId,name,accountNo,uniqTenantId);
        return newUser;
    }

    public void setUserLoggedIn(boolean loggedIn)
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn", loggedIn);
        spEditor.commit();
    }

    public boolean getBoolUserLoggedIn()
    {
        if (userLocalDatabase.getBoolean("loggedIn", false) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void clearUserData()
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}

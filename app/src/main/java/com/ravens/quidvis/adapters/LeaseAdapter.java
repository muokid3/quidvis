package com.ravens.quidvis.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ravens.quidvis.R;
import com.ravens.quidvis.models.Lease;
import com.ravens.quidvis.models.Payment;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 7/2/2016.
 */
public class LeaseAdapter extends ArrayAdapter<Lease> {

    private List<Lease> leases;
    private Context context;
    public LeaseAdapter(Context context, List<Lease> leasesList){
        super(context, R.layout.lease_item,leasesList);
        this.context = context;
        this.leases = leasesList;

    }
    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView tv_title, tv_amount;
        public Button button;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(R.layout.lease_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.tv_amount = (TextView) rowView.findViewById(R.id.tv_amount);
            rowView.setTag(viewHolder);

        }
        // Set text to each TextView of ListView item
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_title.setText(leases.get(position).getName());
        holder.tv_amount.setText(leases.get(position).getValue());

        return rowView;
    }
}

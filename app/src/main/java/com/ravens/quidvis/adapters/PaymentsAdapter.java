package com.ravens.quidvis.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ravens.quidvis.R;
import com.ravens.quidvis.models.Payment;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 7/2/2016.
 */
public class PaymentsAdapter extends ArrayAdapter<Payment> {

    private List<Payment> payments;
    private Context context;
    public PaymentsAdapter(Context context, List<Payment> paymentsList){
        super(context, R.layout.payment_item,paymentsList);
        this.context = context;
        this.payments = paymentsList;

    }
    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView tv_title, tv_details, tv_amount;
        public Button button;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(R.layout.payment_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.tv_details = (TextView) rowView.findViewById(R.id.tv_description);
            viewHolder.tv_amount = (TextView) rowView.findViewById(R.id.tv_amount);
            rowView.setTag(viewHolder);

        }
        // Set text to each TextView of ListView item
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_title.setText(payments.get(position).getTxnDate()+" - "+ payments.get(position).getTxnType());
        holder.tv_details.setText(payments.get(position).getDescr());
        holder.tv_amount.setText("Ksh. "+
                    NumberFormat.getNumberInstance(Locale.US).format(payments.get(position).getCredit()));

        return rowView;
    }
}

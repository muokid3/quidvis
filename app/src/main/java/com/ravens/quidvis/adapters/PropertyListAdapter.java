package com.ravens.quidvis.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.ravens.quidvis.R;
import com.ravens.quidvis.models.Landlord;

import java.util.List;

/**
 * Created by muoki on 7/2/2016.
 */
public class PropertyListAdapter extends ArrayAdapter<Landlord> {

    private List<Landlord> landlords;
    private Context context;
    public PropertyListAdapter(Context context, List<Landlord> landlordListList){
        super(context, R.layout.landlord_item,landlordListList);
        this.context = context;
        this.landlords = landlordListList;

    }
    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView tv_title, tv_details;
        public Button button;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(R.layout.landlord_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.tv_details = (TextView) rowView.findViewById(R.id.tv_description);
            rowView.setTag(viewHolder);

        }
        // Set text to each TextView of ListView item
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_title.setText(landlords.get(position).getName());
        holder.tv_details.setText(landlords.get(position).getAddress().getTown()+", "+landlords.get(position).getAddress().getCountry());
        return rowView;
    }
}

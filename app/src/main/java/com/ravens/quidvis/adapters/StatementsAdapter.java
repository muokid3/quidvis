package com.ravens.quidvis.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.ravens.quidvis.R;
import com.ravens.quidvis.models.RentStatement;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 7/2/2016.
 */
public class StatementsAdapter extends ArrayAdapter<RentStatement> {

    private List<RentStatement> statements;
    private Context context;
    public StatementsAdapter(Context context, List<RentStatement> statementsList){
        super(context, R.layout.statement_item,statementsList);
        this.context = context;
        this.statements = statementsList;

    }
    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView tv_title, tv_details, tv_amount;
        public Button button;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(R.layout.statement_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.tv_details = (TextView) rowView.findViewById(R.id.tv_description);
            viewHolder.tv_amount = (TextView) rowView.findViewById(R.id.tv_amount);
            rowView.setTag(viewHolder);

        }
        // Set text to each TextView of ListView item
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_title.setText(statements.get(position).getTxnDate()+" - "+statements.get(position).getTxnType());
        holder.tv_details.setText(statements.get(position).getDescr());

        int difference = statements.get(position).getDebit() - statements.get(position).getCredit();

        //String textColor = (difference > 0) ? "#000000" : "#ff0000"; //show debit

        if (difference >= 0)
        {
            //its a debit. don't put brackets
            String textColor = "#000000";
            holder.tv_amount.setTextColor(Color.parseColor(textColor));
            holder.tv_amount.setText("Ksh. "+
                    NumberFormat.getNumberInstance(Locale.US).format(statements.get(position).getDebit()));
        }
        else
        {
            //its a credit. put brackets
            String textColor = "#ff0000";
            holder.tv_amount.setTextColor(Color.parseColor(textColor));
            holder.tv_amount.setText("[Ksh. "+
                    NumberFormat.getNumberInstance(Locale.US).format(statements.get(position).getCredit())+
                    "]");
        }
        return rowView;
    }
}

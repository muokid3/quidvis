package com.ravens.quidvis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NewMessageActivity extends AppCompatActivity {

    EditText recipientEmail, tittle, message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        recipientEmail = (EditText)findViewById(R.id.recipientEmail);
        tittle = (EditText)findViewById(R.id.tittle);
        message = (EditText)findViewById(R.id.message);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putCharSequence("recipientEmail", recipientEmail.getText().toString());
        outState.putCharSequence("tittle", tittle.getText().toString());
        outState.putCharSequence("message", message.getText().toString());


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        CharSequence recipientEmail = savedInstanceState.getCharSequence("recipientEmail");
        CharSequence tittle = savedInstanceState.getCharSequence("tittle");
        CharSequence message = savedInstanceState.getCharSequence("message");

        EditText recipientEmailET = (EditText)findViewById(R.id.recipientEmail);
        EditText tittleET = (EditText)findViewById(R.id.tittle);
        EditText messageET = (EditText)findViewById(R.id.message);

        recipientEmailET.setText(recipientEmail);
        tittleET.setText(tittle);
        messageET.setText(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_message_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_send:
                Toast.makeText(this, "Sending...", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.action_settings:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }
}

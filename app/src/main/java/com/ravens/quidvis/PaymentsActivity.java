package com.ravens.quidvis;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.PaymentsAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PaymentsActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;
    public static String PAYMENTS_ENDPOINT = "http://demo.ravens.co.ke/api/lease/payments";
    SweetAlertDialog pDialog, eDialog;
    List<Payment> paymentList;
    ListView listView;
    //TextView countText;
    StringBuilder stringBuilder;
    String paymentsUrl;
    Dialog dialog;
    Button payNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userLocalStore = new UserLocalStore(this);
        paymentList = new ArrayList<>();
        payNow = (Button)findViewById(R.id.pay);

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(PaymentsActivity.this, PayActivity.class));
            }
        });

        listView = (ListView) findViewById(R.id.payment_list);
        //countText = (TextView)findViewById(R.id.count);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(PaymentsActivity.this, Welcome.class));
                finish();
            }
        });

        pDialog.show();

        fetchPayments();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Payment payment = (Payment)listView.getItemAtPosition(position);

                dialog = new Dialog(PaymentsActivity.this);
                dialog.setContentView(R.layout.payments_details);

                TextView txnDate = (TextView) dialog.findViewById(R.id.txnDate);
                txnDate.setText("Transaction date: "+payment.getTxnDate());

                TextView descr = (TextView) dialog.findViewById(R.id.descr);
                descr.setText("Description: "+payment.getDescr());

                TextView accountName = (TextView) dialog.findViewById(R.id.accountName);
                accountName.setText("Account name: "+payment.getAccountName());

                TextView accountCode =(TextView) dialog.findViewById(R.id.accountCode);
                accountCode.setText("Account code: "+payment.getAccountCode());

                TextView posted = (TextView) dialog.findViewById(R.id.posted);
                posted.setText("Posted: "+payment.getPosted());

                TextView txnType = (TextView) dialog.findViewById(R.id.txnType);
                txnType.setText("Transaction type: "+payment.getTxnType());

                TextView refNo = (TextView) dialog.findViewById(R.id.refNo);
                refNo.setText("Reference number: "+payment.getRefNo());

                TextView txnNo =(TextView) dialog.findViewById(R.id.txnNo);
                txnNo.setText("Transaction number: "+payment.getTxnNo());

                TextView user = (TextView) dialog.findViewById(R.id.user);
                user.setText("User: "+payment.getUser());

                TextView credit = (TextView) dialog.findViewById(R.id.credit);
                credit.setText("Credit: "+payment.getCredit());

                TextView balance = (TextView) dialog.findViewById(R.id.balance);
                balance.setText("Balance: "+payment.getBalance());

                TextView accountId = (TextView) dialog.findViewById(R.id.accountId);
                accountId.setText("Account ID: "+payment.getAccountId());

                TextView journalId = (TextView) dialog.findViewById(R.id.journalId);
                journalId.setText("Journal ID: "+payment.getJournalId());

                Button closeButton;
                closeButton = (Button) dialog.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(PaymentsActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }


    private void fetchPayments()
    {
        stringBuilder = new StringBuilder(PAYMENTS_ENDPOINT);

        stringBuilder.append("?companyId="+userLocalStore.getLoggedInUser().getCompanyId());
        stringBuilder.append("&accountId="+userLocalStore.getLoggedInUser().getLedgerId());

        paymentsUrl = stringBuilder.toString();

        StringRequest req = new StringRequest(Request.Method.GET, paymentsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONObject responseObject = new JSONObject(response);
                            //int count = responseObject.getInt("count");
                            //countText.setText("Total: "+count);


                            JSONArray itemDetailsArray = responseObject.getJSONArray("list");

                            for (int i = 0; i < itemDetailsArray.length(); i++) {


                                JSONObject paymentItem = (JSONObject) itemDetailsArray.get(i);

                                String txnDate = paymentItem.getString("txnDate");
                                String descr = paymentItem.getString("descr");
                                String accountName = paymentItem.getString("accountName");
                                String accountCode = paymentItem.getString("accountCode");
                                String posted = paymentItem.getString("posted");
                                String txnType = paymentItem.getString("txnType");
                                String refNo = paymentItem.getString("refNo");
                                String txnNo = paymentItem.getString("txnNo");
                                String user = paymentItem.getString("user");
                                int id = paymentItem.getInt("id");
                                int debit = paymentItem.getInt("debit");
                                int credit = paymentItem.getInt("credit");
                                int balance = paymentItem.getInt("balance");
                                int accountId = paymentItem.getInt("accountId");
                                int journalId = paymentItem.getInt("journalId");

                                Payment payment = new Payment(txnDate,descr,accountName,accountCode,posted,txnType,refNo,txnNo,user,id,debit,credit,balance,accountId,journalId);

                                paymentList.add(payment);

                            }

                            ArrayAdapter<Payment> adapter = new PaymentsAdapter(PaymentsActivity.this, paymentList);
                            listView.setAdapter(adapter);

                            pDialog.dismiss();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(PaymentsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(PaymentsActivity.this, Welcome.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(PaymentsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, PaymentsActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(PaymentsActivity.this, Welcome.class));
                                finish();
                            }

                        })
                        .show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(PaymentsActivity.this);
        requestQueue.add(req);

    }
}

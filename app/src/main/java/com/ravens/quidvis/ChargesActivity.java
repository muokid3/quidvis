package com.ravens.quidvis;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.ChargeAdapter;
import com.ravens.quidvis.adapters.PaymentsAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.Charge;
import com.ravens.quidvis.models.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChargesActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;
    public static String CHARGES_ENDPOINT = "http://demo.ravens.co.ke/api/lease/charges";
    SweetAlertDialog pDialog, eDialog;

    List<Charge> chargesList;
    ListView listView;
    //TextView countText;
    StringBuilder stringBuilder;
    String chargesUrl;
    Dialog dialog;
    Button pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charges);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userLocalStore = new UserLocalStore(this);
        chargesList = new ArrayList<>();
        pay = (Button)findViewById(R.id.pay);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ChargesActivity.this, PayActivity.class));
            }
        });

        listView = (ListView) findViewById(R.id.charge_list);
        //countText = (TextView)findViewById(R.id.count);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(ChargesActivity.this, Welcome.class));
                finish();
            }
        });

        pDialog.show();

        fetchCharges();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Charge charge = (Charge)listView.getItemAtPosition(position);

                dialog = new Dialog(ChargesActivity.this);
                dialog.setContentView(R.layout.charges_details);

                TextView txnDate = (TextView) dialog.findViewById(R.id.txnDate);
                txnDate.setText("Transaction date: "+charge.getTxnDate());

                TextView descr = (TextView) dialog.findViewById(R.id.descr);
                descr.setText("Description: "+charge.getDescr());

                TextView accountName = (TextView) dialog.findViewById(R.id.accountName);
                accountName.setText("Account name: "+charge.getAccountName());

                TextView accountCode =(TextView) dialog.findViewById(R.id.accountCode);
                accountCode.setText("Account code: "+charge.getAccountCode());

                TextView posted = (TextView) dialog.findViewById(R.id.posted);
                posted.setText("Posted: "+charge.getPosted());

                TextView txnType = (TextView) dialog.findViewById(R.id.txnType);
                txnType.setText("Transaction type: "+charge.getTxnType());

                TextView refNo = (TextView) dialog.findViewById(R.id.refNo);
                refNo.setText("Reference number: "+charge.getRefNo());

                TextView txnNo =(TextView) dialog.findViewById(R.id.txnNo);
                txnNo.setText("Transaction number: "+charge.getTxnNo());

                TextView credit = (TextView) dialog.findViewById(R.id.credit);
                credit.setText("Debit: "+charge.getDebit());

                TextView balance = (TextView) dialog.findViewById(R.id.balance);
                balance.setText("Balance: "+charge.getBalance());

                TextView accountId = (TextView) dialog.findViewById(R.id.accountId);
                accountId.setText("Account ID: "+charge.getAccountId());

                TextView journalId = (TextView) dialog.findViewById(R.id.journalId);
                journalId.setText("Journal ID: "+charge.getJournalId());

                Button closeButton;
                closeButton = (Button) dialog.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(ChargesActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private void fetchCharges()
    {
        stringBuilder = new StringBuilder(CHARGES_ENDPOINT);

        stringBuilder.append("?companyId="+userLocalStore.getLoggedInUser().getCompanyId());
        stringBuilder.append("&accountId="+userLocalStore.getLoggedInUser().getLedgerId());

        chargesUrl = stringBuilder.toString();

        StringRequest req = new StringRequest(Request.Method.GET, chargesUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONObject responseObject = new JSONObject(response);
                            //int count = responseObject.getInt("count");
                            //countText.setText("Total: "+count);


                            JSONArray itemDetailsArray = responseObject.getJSONArray("list");

                            for (int i = 0; i < itemDetailsArray.length(); i++) {

                                JSONObject chargeItem = (JSONObject) itemDetailsArray.get(i);

                                String txnDate = chargeItem.getString("txnDate");
                                String descr = chargeItem.getString("descr");
                                String accountName = chargeItem.getString("accountName");
                                String accountCode = chargeItem.getString("accountCode");
                                String posted = chargeItem.getString("posted");
                                String txnType = chargeItem.getString("txnType");
                                String refNo = chargeItem.getString("refNo");
                                String txnNo = chargeItem.getString("txnNo");
                                int id = chargeItem.getInt("id");
                                int debit = chargeItem.getInt("debit");
                                int credit = chargeItem.getInt("credit");
                                int balance = chargeItem.getInt("balance");
                                int accountId = chargeItem.getInt("accountId");
                                int journalId = chargeItem.getInt("journalId");


                                Charge charge = new Charge(txnDate,descr,accountName,accountCode,posted,txnType,refNo,txnNo,id,debit,credit,balance,accountId,journalId);

                                chargesList.add(charge);

                            }

                            ArrayAdapter<Charge> adapter = new ChargeAdapter(ChargesActivity.this, chargesList);
                            listView.setAdapter(adapter);

                            pDialog.dismiss();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(ChargesActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(ChargesActivity.this, Welcome.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(ChargesActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, ChargesActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(ChargesActivity.this, Welcome.class));
                                finish();
                            }

                        })
                        .show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(ChargesActivity.this);
        requestQueue.add(req);

    }
}

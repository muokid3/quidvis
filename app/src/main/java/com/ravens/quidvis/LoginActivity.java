package com.ravens.quidvis;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    Button login;
    SweetAlertDialog pDialog, eDialog;
    public static String LOGIN_ENDPOINT = "http://demo.ravens.co.ke/api/auth/login";
    HashMap<String, String> params;
    JSONObject payload;
    UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userLocalStore = new UserLocalStore(this);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        login = (Button)findViewById(R.id.login);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Logging In");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setContentText("Invalid login details");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                finish();
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateEditTexts())
                {
                    pDialog.show();

                    Map<String, String> params = new HashMap<String, String>();

                    //Pass Payload as JSON object like this

                    params.put("username", username.getText().toString());
                    params.put("password", password.getText().toString());

                    payload= new JSONObject(params);
                    doLogin();
                }

            }
        });
    }

    private boolean validateEditTexts()
    {
        boolean valid = true;

        if(username.getText().toString().trim().equals("") )
        {
            username.setError("Please provide your username");
            valid = false;
        }

        if(password.getText().toString().trim().equals(""))
        {
            password.setError("Please provide your password");
            valid = false;
        }
        return valid;
    }

    private void doLogin()
    {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, LOGIN_ENDPOINT , payload,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            pDialog.dismiss();

                            String token  = response.getString("token");
                            String name  = response.getString("name");
                            String userId  = response.getString("userId");
                            String landlordId  = response.getString("landlordId");
                            String tenantId  = response.getString("tenantId");
                            String leaseId  = response.getString("leaseId");
                            String ledgerId  = response.getString("ledgerId");
                            String companyId  = response.getString("companyId");

                            String accountNo  = response.getString("accountNo");
                            String uniqTenantId  = response.getString("uniqTenantId");



                            if (token != null)
                            {
                                User returnedUser = new User(username.getText().toString(),password.getText().toString(),token,userId,landlordId,tenantId,leaseId,ledgerId,companyId,name,accountNo,uniqTenantId);
                                logUserIn(returnedUser);
                            }
                            else
                            {
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Response")
                                        .setContentText(response.toString())
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                                                finish();
                                            }

                                        })
                                        .show();
                            }
                            //VolleyLog.v("Response:%n %s", response.toString(4));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.e("Error: ", error.getMessage());
                pDialog.dismiss();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, LoginActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                                finish();
                            }

                        })
                        .show();
            }
        }){
            @Override
            public byte[] getBody() {
                return payload.toString().getBytes();

            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        requestQueue.add(req);

    }

    private void logUserIn(User returnedUser)
    {
        userLocalStore.storeUserData(returnedUser);
        userLocalStore.setUserLoggedIn(true);
        startActivity(new Intent(LoginActivity.this, Welcome.class));
        finish();
    }
}

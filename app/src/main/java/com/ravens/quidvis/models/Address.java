package com.ravens.quidvis.models;

/**
 * Created by muoki on 8/29/2016.
 */
public class Address {
    String road, town, country , location;

    public Address(String road, String town, String country, String location) {
        this.road = road;
        this.town = town;
        this.country = country;
        this.location = location;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

package com.ravens.quidvis.models;

/**
 * Created by muoki on 2/1/2016.
 */
public class User {
    public String username;
    public String password;
    public String token;
    public String userId;
    public String landlordId;
    public String tenantId;
    public String leaseId;
    public String ledgerId;
    public String companyId;
    public String name;

    public String accountNo;
    public String uniqTenantId;


    public User(String username, String password, String token, String userId, String landlordId, String tenantId, String leaseId, String ledgerId, String companyId, String name, String accountNo, String uniqTenantId) {
        this.username = username;
        this.password = password;
        this.token = token;
        this.userId = userId;
        this.landlordId = landlordId;
        this.tenantId = tenantId;
        this.leaseId = leaseId;
        this.ledgerId = ledgerId;
        this.companyId = companyId;
        this.name = name;
        this.accountNo = accountNo;
        this.uniqTenantId = uniqTenantId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getUniqTenantId() {
        return uniqTenantId;
    }

    public void setUniqTenantId(String uniqTenantId) {
        this.uniqTenantId = uniqTenantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(String landlordId) {
        this.landlordId = landlordId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getLeaseId() {
        return leaseId;
    }

    public void setLeaseId(String leaseId) {
        this.leaseId = leaseId;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

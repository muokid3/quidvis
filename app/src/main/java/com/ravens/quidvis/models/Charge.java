package com.ravens.quidvis.models;

/**
 * Created by Dennis Muoki on 9/13/2016.
 */
public class Charge {
    String txnDate,descr,accountName,accountCode,posted,txnType,refNo,txnNo;
    int id,debit,credit,balance,accountId,journalId;

    public Charge(String txnDate, String descr, String accountName, String accountCode, String posted, String txnType, String refNo, String txnNo, int id, int debit, int credit, int balance, int accountId, int journalId) {
        this.txnDate = txnDate;
        this.descr = descr;
        this.accountName = accountName;
        this.accountCode = accountCode;
        this.posted = posted;
        this.txnType = txnType;
        this.refNo = refNo;
        this.txnNo = txnNo;
        this.id = id;
        this.debit = debit;
        this.credit = credit;
        this.balance = balance;
        this.accountId = accountId;
        this.journalId = journalId;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getTxnNo() {
        return txnNo;
    }

    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDebit() {
        return debit;
    }

    public void setDebit(int debit) {
        this.debit = debit;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }
}

package com.ravens.quidvis.models;

/**
 * Created by muoki on 10/10/16.
 */

public class Lease {
    String name, value;

    public Lease(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

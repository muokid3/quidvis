package com.ravens.quidvis.models;

/**
 * Created by muoki on 9/9/2016.
 */
public class RentStatement {
    String accountName, descr, txnType, txnDate;
    int credit, debit;

    public RentStatement(String accountName, String descr, String txnType, String txnDate, int credit, int debit) {
        this.accountName = accountName;
        this.descr = descr;
        this.txnType = txnType;
        this.txnDate = txnDate;
        this.credit = credit;
        this.debit = debit;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getDebit() {
        return debit;
    }

    public void setDebit(int debit) {
        this.debit = debit;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }
}

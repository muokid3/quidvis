package com.ravens.quidvis.models;

/**
 * Created by muoki on 8/29/2016.
 */
public class Landlord {
    String occUnits,vacUnits,landlordName,clientId,landlordAddr,landlordLocation,id,code,name,lrNumber,category,rentSale,flrs,buildngTp,unitOfMeasure,accountModeLevel;
    Address address;

    public Landlord(String occUnits, String vacUnits, String landlordName, String clientId, String landlordAddr, String landlordLocation, String id, String code, String name, String lrNumber, String category, String rentSale, String flrs, String buildngTp, String unitOfMeasure, String accountModeLevel, Address address) {
        this.occUnits = occUnits;
        this.vacUnits = vacUnits;
        this.landlordName = landlordName;
        this.clientId = clientId;
        this.landlordAddr = landlordAddr;
        this.landlordLocation = landlordLocation;
        this.id = id;
        this.code = code;
        this.name = name;
        this.lrNumber = lrNumber;
        this.category = category;
        this.rentSale = rentSale;
        this.flrs = flrs;
        this.buildngTp = buildngTp;
        this.unitOfMeasure = unitOfMeasure;
        this.accountModeLevel = accountModeLevel;
        this.address = address;
    }

    public String getOccUnits() {
        return occUnits;
    }

    public void setOccUnits(String occUnits) {
        this.occUnits = occUnits;
    }

    public String getVacUnits() {
        return vacUnits;
    }

    public void setVacUnits(String vacUnits) {
        this.vacUnits = vacUnits;
    }

    public String getLandlordName() {
        return landlordName;
    }

    public void setLandlordName(String landlordName) {
        this.landlordName = landlordName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLandlordAddr() {
        return landlordAddr;
    }

    public void setLandlordAddr(String landlordAddr) {
        this.landlordAddr = landlordAddr;
    }

    public String getLandlordLocation() {
        return landlordLocation;
    }

    public void setLandlordLocation(String landlordLocation) {
        this.landlordLocation = landlordLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRentSale() {
        return rentSale;
    }

    public void setRentSale(String rentSale) {
        this.rentSale = rentSale;
    }

    public String getFlrs() {
        return flrs;
    }

    public void setFlrs(String flrs) {
        this.flrs = flrs;
    }

    public String getBuildngTp() {
        return buildngTp;
    }

    public void setBuildngTp(String buildngTp) {
        this.buildngTp = buildngTp;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getAccountModeLevel() {
        return accountModeLevel;
    }

    public void setAccountModeLevel(String accountModeLevel) {
        this.accountModeLevel = accountModeLevel;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}

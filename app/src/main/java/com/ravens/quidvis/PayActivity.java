package com.ravens.quidvis;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.ravens.quidvis.dependencies.Displays;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PayActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;
    public static String MPESA_ENDPOINT = "http://lostandfounddocumentscentre.com/mpesa/requestcheckout.php";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_PHONE = "number";
    SweetAlertDialog pDialog, eDialog, sDialog, wDialog;

    Button continueButton;
    RadioButton payTypeButton;
    RadioGroup payTypeGroup;
    EditText amount, phoneNo;

    PayPalConfiguration m_configuration;
    // the id is the link to the paypal account, we have to create an app and get its id
    String m_paypalClientId = "Ab6_JzKJZgaSRIRiWfim812pm6Hg9-xrULQrqumMm12kgpmMO3tGaiLVZ_UP7WZ0dVVe1it0SUZKLdB_";
    Intent m_service;
    int m_paypalRequestCode = 999; // or any number you want

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        m_configuration = new PayPalConfiguration()
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(m_paypalClientId);

        m_service = new Intent(this, PayPalService.class);
        m_service.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration);
        startService(m_service);


        userLocalStore = new UserLocalStore(this);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Processing...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Ok");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(PayActivity.this, Welcome.class));
                finish();
            }
        });

        sDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sDialog.setTitleText("Success!");
        sDialog.setConfirmText("Ok");
        sDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(PayActivity.this, Welcome.class));
                finish();
            }
        });

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.setTitleText("Warning!");
        wDialog.setConfirmText("Ok");
        wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(PayActivity.this, Welcome.class));
                finish();
            }
        });

        payTypeGroup = (RadioGroup) findViewById(R.id.payTypeRadioGroup);
        amount = (EditText) findViewById(R.id.amount);
        phoneNo = (EditText) findViewById(R.id.phoneNo);
        continueButton = (Button) findViewById(R.id.continueButton);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selected = payTypeGroup.getCheckedRadioButtonId();
                payTypeButton = (RadioButton) findViewById(selected);

               if (payTypeButton.getText().toString().equals("MPESA")) {
                    //process mpesa here

                    if (validateMpesa()) {
                        processMpesa();
                    }

                } else if (payTypeButton.getText().toString().equals("PAYPAL/CARD")) {
                    //do card and paypal here
                    if (validateCard()) {
                        processCard(Integer.parseInt(amount.getText().toString()));
                    }
                }

            }
        });
    }

    void processCard(Integer amount)
    {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(amount), "USD", "Test payment with Paypal",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, m_paypalRequestCode);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == m_paypalRequestCode)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                // we have to confirm that the payment worked to avoid fraud
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if(confirmation != null)
                {
                    String state = confirmation.getProofOfPayment().getState();

                    if(state.equals("approved")) // if the payment worked, the state equals approved
                    {
                        sDialog.setContentText("Payment approved");
                        sDialog.show();
                    }
                    else
                    {
                        eDialog.setContentText("An error occurred in the payment");
                        eDialog.show();
                    }
                }
                else
                {
                    wDialog.setContentText("Transaction confirmation is null");
                    wDialog.show();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate() == false) {
            startActivity(new Intent(PayActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate() {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private boolean validateMpesa() {
        boolean valid = true;

        if (phoneNo.getText().toString().trim().equals("")) {
            phoneNo.setError("Please provide a phone number");
            valid = false;
        }

        if (amount.getText().toString().trim().equals("")) {
            amount.setError("Please provide the amount to pay");
            valid = false;
        }

        return valid;
    }

    private boolean validateCard() {
        boolean valid = true;

        if (amount.getText().toString().trim().equals("")) {
            amount.setError("Please provide the amount to pay");
            valid = false;
        }

        return valid;
    }

    private void processMpesa()
    {
        pDialog.show();

        StringRequest req = new StringRequest(Request.Method.POST, MPESA_ENDPOINT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        pDialog.dismiss();
                        startActivity(new Intent(PayActivity.this, Welcome.class));
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                pDialog.dismiss();

                Displays.displayErrorAlert("Error", VolleyErrors.getVolleyErrorMessages(error, PayActivity.this), PayActivity.this);

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map <String, String> params = new HashMap<String, String>();
                params.put(KEY_AMOUNT, amount.getText().toString());
                params.put(KEY_PHONE, phoneNo.getText().toString());

                return params;
            }
        };

        //req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(PayActivity.this);
        requestQueue.add(req);

    }


}

package com.ravens.quidvis;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.PropertyListAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.dependencies.VolleyErrors;
import com.ravens.quidvis.models.Address;
import com.ravens.quidvis.models.Landlord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PropertyActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;
    public static String PROPERTY_LIST_ENDPOINT = "http://demo.ravens.co.ke/api/property/list?companyId=1906";
    SweetAlertDialog pDialog, eDialog;
    List<Landlord> landlordList;
    ListView listView;
    //TextView countText;
    Button payNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userLocalStore = new UserLocalStore(this);
        landlordList = new ArrayList<>();

        payNow = (Button)findViewById(R.id.pay);

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(PropertyActivity.this, PayActivity.class));
            }
        });

        listView = (ListView) findViewById(R.id.landlord_list);
        //countText = (TextView)findViewById(R.id.count);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(PropertyActivity.this, Welcome.class));
                finish();
            }
        });

        pDialog.show();

        fetchLandlords();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(PropertyActivity.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private void fetchLandlords()
    {
        StringRequest req = new StringRequest(Request.Method.GET, PROPERTY_LIST_ENDPOINT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONObject responseObject = new JSONObject(response);
                            //int count = responseObject.getInt("count");
                            //countText.setText("Total: "+count);


                            JSONArray itemDetailsArray = responseObject.getJSONArray("list");

                            for (int i = 0; i < itemDetailsArray.length(); i++) {


                                JSONObject landlordItem = (JSONObject) itemDetailsArray.get(i);
                                //Toast.makeText(StatementsActivity.this,landlordItem.toString(), Toast.LENGTH_LONG ).show();

                                String occUnits = landlordItem.getInt("occUnits")+"";
                                String vacUnits = landlordItem.getInt("vacUnits")+"";
                                String landlordName = landlordItem.getString("landlordName");
                                String clientId = landlordItem.getInt("clientId")+"";
                                String landlordAddr = "ADDR";//landlordItem.getString("landlordAddr");
                                String landlordLocation = "LOC";//landlordItem.getString("landlordLocation");
                                String id = landlordItem.getInt("id")+"";
                                String code = landlordItem.getString("code");
                                String name = landlordItem.getString("name");
                                String lrNumber = landlordItem.getString("lrNumber");
                                String rentSale = landlordItem.getString("rentSale");
                                String category = landlordItem.getString("category");
                                String flrs = landlordItem.getInt("flrs")+"";
                                String buildngTp = landlordItem.getString("buildngTp");
                                String unitOfMeasure = "Unit";//landlordItem.getString("unitOfMeasure");
                                String accountModeLevel = landlordItem.getString("accountModeLevel");

                                JSONObject address = landlordItem.getJSONObject("address");
                                String road = address.getString("road");
                                String town = address.getString("town");
                                String country = address.getString("country");
                                String location = address.getString("location");

                                Address landlordAddress = new Address(road, town, country, location);

                                Landlord landlord = new Landlord(occUnits,vacUnits,landlordName,clientId,landlordAddr,landlordLocation,id,code,name,lrNumber,category,rentSale,flrs,buildngTp,unitOfMeasure,accountModeLevel,landlordAddress);

                                landlordList.add(landlord);

                            }

                            ArrayAdapter<Landlord> adapter = new PropertyListAdapter(PropertyActivity.this, landlordList);
                            listView.setAdapter(adapter);

                            pDialog.dismiss();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(PropertyActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(PropertyActivity.this, Welcome.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(PropertyActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, PropertyActivity.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(PropertyActivity.this, Welcome.class));
                                finish();
                            }

                        })
                        .show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);

                //String crude = "NDM4YTRjODUtYzQ2NS00ZmFhLWE5YTItZDE0ZGIzZTRhZjIzOjQzOGE0Yzg1LWM0NjUtNGZhYS1hOWEyLWQxNGRiM2U0YWYyMw\n" +
                //        "==";
                Map<String, String>  params = new HashMap<String, String>();
                //params.put("token",token);
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(PropertyActivity.this);
        requestQueue.add(req);

    }
}

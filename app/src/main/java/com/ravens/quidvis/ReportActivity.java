package com.ravens.quidvis;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ReportActivity extends AppCompatActivity {

    Button b1,b2,btnReport;
    ImageView iv;
    SweetAlertDialog pDialog, sDialog;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        b1=(Button)findViewById(R.id.button);
        btnReport=(Button)findViewById(R.id.btnReport);
        iv=(ImageView)findViewById(R.id.imageView);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Reporting...");
        pDialog.setCancelable(false);

        sDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sDialog.setTitleText("Success!");
        sDialog.setContentText("Reported successfully.");
        sDialog.setConfirmText("Ok");
        sDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                finish();
                startActivity(new Intent(ReportActivity.this, Welcome.class));
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ReportActivity.this, "Reporting...", Toast.LENGTH_LONG).show();
                pDialog.show();
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        doStuff();
                    }
                }, 3000);

            }
        });
    }

    private void doStuff() {
        pDialog.dismiss();
        sDialog.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bp = (Bitmap) data.getExtras().get("data");
        iv.setImageBitmap(bp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

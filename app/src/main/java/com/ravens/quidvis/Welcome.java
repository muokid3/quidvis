package com.ravens.quidvis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.widget.GridView;


import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ravens.quidvis.adapters.CustomGridViewAdapter;
import com.ravens.quidvis.dependencies.UserLocalStore;
import com.ravens.quidvis.models.Item;

public class Welcome extends AppCompatActivity {
    GridView gridView;
    ArrayList<Item> gridArray = new ArrayList<Item>();
    CustomGridViewAdapter customGridAdapter;
    TextView totalTv;

    UserLocalStore userLocalStore;
    public static String TOTAL_BASE_URL = "http://demo.ravens.co.ke/api/lease/balance";

    StringBuilder stringBuilder;
    String statementsUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        totalTv = (TextView)findViewById(R.id.total);
        userLocalStore = new UserLocalStore(this);

        //Bitmap homeIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.home);
        Bitmap chargesIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.charges);
        Bitmap propertiesIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.property);
        Bitmap statementsIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.statements);
        Bitmap paymentsIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.payments);
        Bitmap depositsIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.security_deposits);
        Bitmap reportIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.report);
        Bitmap leaseIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.lease_details);
        Bitmap logoutIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.logout);
        Bitmap payIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_pay);
        Bitmap mailIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_mail);
        //Bitmap userIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.personal);
        gridArray.add(new Item(payIcon, "Pay"));
        gridArray.add(new Item(statementsIcon, "Statements"));
        gridArray.add(new Item(chargesIcon, "Charges"));
        gridArray.add(new Item(paymentsIcon, "Payments"));
        gridArray.add(new Item(leaseIcon, "Lease Details"));
        gridArray.add(new Item(reportIcon, "Report"));
        gridArray.add(new Item(propertiesIcon, "Properties"));
        gridArray.add(new Item(mailIcon, "Inbox"));
        gridArray.add(new Item(logoutIcon, "Logout"));


        gridView = (GridView) findViewById(R.id.gridView1);
        customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid, gridArray);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String action = gridArray.get(position).getTitle();

                switch (action)
                {
                    case "Charges":
                        startActivity(new Intent(Welcome.this, ChargesActivity.class));
                        break;
                    case "Lease Details":
                        startActivity(new Intent(Welcome.this, LeaseDetailsActivity.class));
                        break;
                    case "Pay":
                        startActivity(new Intent(Welcome.this, PayActivity.class));
                        break;
                    case "Payments":
                        startActivity(new Intent(Welcome.this, PaymentsActivity.class));
                        break;
                    case "Properties":
                        startActivity(new Intent(Welcome.this, PropertyActivity.class));
                        break;
                    case "Statements":
                        startActivity(new Intent(Welcome.this, StatementsActivity.class));
                        break;
                    case "Report":
                        startActivity(new Intent(Welcome.this, ReportActivity.class));
                        break;
                    case "Inbox":
                        startActivity(new Intent(Welcome.this, InboxActivity.class));
                        break;
                    case "Logout":
                        userLocalStore.clearUserData();
                        startActivity(new Intent(Welcome.this, LoginActivity.class));
                        finish();
                        break;
                }
            }
        });

        fetchTotal();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()==false)
        {
            startActivity(new Intent(Welcome.this, LoginActivity.class));
            finish();
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getBoolUserLoggedIn();
    }

    private void fetchTotal()
    {
        stringBuilder = new StringBuilder(TOTAL_BASE_URL);

        stringBuilder.append("?companyId="+userLocalStore.getLoggedInUser().getCompanyId());
        stringBuilder.append("&accountId="+userLocalStore.getLoggedInUser().getLedgerId());

        statementsUrl = stringBuilder.toString();

        StringRequest req = new StringRequest(Request.Method.GET, statementsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String total = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(response));
                        totalTv.setText("Tenant Balance: "+total);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
               //Toast.makeText(Welcome.this, VolleyErrors.getVolleyErrorMessages(error, Welcome.this), Toast.LENGTH_SHORT).show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token  = userLocalStore.getLoggedInUser().token;
                byte[] encodeValue = Base64.encode(token.getBytes(), Base64.DEFAULT);
                String authorization = new String(encodeValue);
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic "+authorization);

                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(Welcome.this);
        requestQueue.add(req);

    }


}
